<?php

namespace Drupal\xslideshow;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\xslideshow\Entity\Slide;
use Drupal\xslideshow\Entity\SlideType;

class XslideshowController extends ControllerBase {

  /**
   * Slide "add links" page.
   */
  public function slideAddLinks(): array {
    $content = [];

    foreach (SlideType::loadMultiple() as $slide_type) {
      $content[$slide_type->id()] = [
        'title' => $slide_type->label(),
        'url' => Url::fromRoute('entity.slide.add_form', ['slide_type' => $slide_type->id()]),
        'localized_options' => [],
      ];
    }

    return [
      '#theme' => 'admin_block_content',
      '#content' => $content,
    ];
  }

  /**
   * Slide "add form" page.
   */
  public function slideAddForm(SlideType $slide_type): array {
    $slide = Slide::create([
      'type' => $slide_type->id(),
    ]);
    return $this->entityFormBuilder()->getForm($slide);
  }

}
