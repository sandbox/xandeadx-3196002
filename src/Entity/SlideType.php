<?php

namespace Drupal\xslideshow\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * @ConfigEntityType(
 *   id = "slide_type",
 *   label = @Translation("Slide type"),
 *   config_prefix = "slide_type",
 *   bundle_of = "slide",
 *   admin_permission = "administer nodes",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   handlers = {
 *     "list_builder" = "\Drupal\xslideshow\SlideTypeListBuilder",
 *     "form" = {
 *       "default" = "\Drupal\xslideshow\Form\SlideTypeForm",
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/content/slides/types",
 *     "add-form" = "/admin/content/slides/types/add",
 *     "edit-form" = "/admin/content/slides/types/edit/{slide_type}",
 *     "delete-form" = "/admin/content/slides/types/delete/{slide_type}",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 * )
 */
class SlideType extends ConfigEntityBundleBase {

  protected $id;

  protected $label;

}
