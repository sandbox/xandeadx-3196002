<?php

namespace Drupal\xslideshow\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\link\LinkItemInterface;

/**
 * @ContentEntityType(
 *   id = "slide",
 *   label = @Translation("Slide"),
 *   base_table = "slide",
 *   data_table = "slide_field_data",
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   field_ui_base_route = "entity.slide_type.edit_form",
 *   common_reference_target = TRUE,
 *   bundle_entity_type = "slide_type",
 *   bundle_label = @Translation("Slide type"),
 *   admin_permission = "administer nodes",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "published" = "published",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "\Drupal\xslideshow\Form\SlideForm",
 *       "delete" = "\Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "\Drupal\xslideshow\SlideListBuilder",
 *     "access" = "\Drupal\xslideshow\SlideAccessControlHandler",
 *   },
 *   links = {
 *     "collection" = "/admin/content/slides",
 *     "add-form" = "/admin/content/slides/add",
 *     "edit-form" = "/admin/content/slides/edit/{slide}",
 *     "canonical" = "/admin/content/slides/edit/{slide}",
 *     "delete-form" = "/admin/content/slides/delete/{slide}",
 *   },
 * )
 */
class Slide extends ContentEntityBase implements EntityPublishedInterface {

  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type); /** @var BaseFieldDefinition[] $fields */
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Body'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('URL'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_DISABLED,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'file_directory' => 'upload/slide-images',
        'alt_field' => 0,
        'alt_field_required' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mobile_image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Mobile image'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'file_directory' => 'slide-images',
        'alt_field' => 0,
        'alt_field_required' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['published']->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    if (!$this->label()) {
      $this->set('title', t('Slide'));
    }

    parent::preSave($storage);
  }

}
