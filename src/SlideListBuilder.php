<?php

namespace Drupal\xslideshow;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\xslideshow\Entity\Slide;

class SlideListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [
      'id' => 'ID',
      'title' => $this->t('Title'),
      'type' => $this->t('Type'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var Slide $entity */

    $row = [
      'id' => $entity->id(),
      'title' => $entity->label(),
      'type' => $entity->get('type')->entity->label(),
    ];

    return $row + parent::buildRow($entity);
  }

}
