<?php

namespace Drupal\xslideshow\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xslideshow\Entity\SlideType;

class SlideTypeForm extends EntityForm {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $slide_type = $this->entity; /** @var SlideType $slide_type */

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID'),
      '#default_value' => $slide_type->id(),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $slide_type->label(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $this->messenger()->addStatus(t('Slide type added.'));
    $form_state->setRedirect('entity.slide_type.collection');
    return parent::save($form, $form_state);
  }

}
